# 无监督算法实现

- 社区：[Mars](https://docs.pymars.org/en/latest/)

- 项目链接：[无监督算法实现](https://summer.iscas.ac.cn/#/org/prodetail/211080749)



# 成果展示

只包含项目中本人更新文件，文件目录与项目路径相同

### MiniBatch KMeans

- `mars/mars/learn/cluster/_mini_batch_k_means.py`
- `mars/mars/learn/cluster/_mini_batch_k_means_operand.py`

## Agglomerative

- `mars/mars/learn/cluster/_agglomerative.py`
- `mars/mars/learn/cluster/_agglomerative_operand.py`

## Test

- `mars/mars/learn/cluster/tests/test_mini_batch_k_means.py`
- `mars/mars/learn/cluster/tests/test_agglomerative.py`